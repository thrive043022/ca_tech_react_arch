{
  "root": true,
  "settings": {
    "react": {
      "createClass": "createReactClass", // Regex for Component Factory to use,
                                         // default to "createReactClass"
      "pragma": "React",  // Pragma to use, default to "React"
      "version": "detect", // React version. "detect" automatically picks the version you have installed.
                           // You can also use `16.0`, `16.3`, etc, if you want to override the detected value.
                           // default to latest and warns if missing
                           // It will default to "detect" in the future
      "flowVersion": "0.53" // Flow version
    },
    "propWrapperFunctions": [
        // The names of any function used to wrap propTypes, e.g. `forbidExtraProps`. If this isn't set, any propTypes wrapped in a function will be skipped.
        "forbidExtraProps",
        {"property": "freeze", "object": "Object"},
        {"property": "myFavoriteWrapper"}
    ],
    "linkComponents": [
      // Components used as alternatives to <a> for linking, eg. <Link to={ url } />
      "Hyperlink",
      {"name": "Link", "linkAttribute": "to"}
    ]
  },
  "plugins": [
    "eslint-plugin",
    "@typescript-eslint",
    "react",
    "jest",
    "import",
    "eslint-comments",
    "prettier"
  ],
  "env": {
    "es6": true,
    "node": true
  },
  
  "extends": [
    "plugin:react/recommended",
    "eslint:recommended",
    "plugin:@typescript-eslint/eslint-recommended",
    "plugin:@typescript-eslint/recommended",
    "plugin:@typescript-eslint/recommended-requiring-type-checking",
    "plugin:prettier/recommended"
   
  ],
  "rules": {
    //
    // our plugin :D
    //
    "react/jsx-filename-extension": [1, {
      "extensions": [".ts", ".tsx", ".js", ".jsx"]
    }],
    "@typescript-eslint/consistent-type-definitions": ["error", "interface"],
    "@typescript-eslint/explicit-function-return-type": "off",
   // "@typescript-eslint/no-explicit-any": "error",
    "@typescript-eslint/no-non-null-assertion": "off",
    "@typescript-eslint/no-use-before-define": "off",
    "@typescript-eslint/no-var-requires": "off",
    "@typescript-eslint/unbound-method": "off",
    "prettier/prettier": "error",

    //
    // eslint base
    //

    "comma-dangle": ["error", "always-multiline"],
    "curly": ["error", "all"],
    "no-mixed-operators": "error",
    //"no-console": "error",
    "no-process-exit": "error",

    //
    // eslint-plugin-eslint-comment
    //

    // require a eslint-enable comment for every eslint-disable comment
    "eslint-comments/disable-enable-pair": [
      "error",
      {
        "allowWholeFile": true
      },
    ],
    // disallow a eslint-enable comment for multiple eslint-disable comments
    "eslint-comments/no-aggregating-enable": "error",
    // disallow duplicate eslint-disable comments
    "eslint-comments/no-duplicate-disable": "error",
    // disallow eslint-disable comments without rule names
    "eslint-comments/no-unlimited-disable": "error",
    // disallow unused eslint-disable comments
    "eslint-comments/no-unused-disable": "error",
    // disallow unused eslint-enable comments
    "eslint-comments/no-unused-enable": "error",
    // disallow ESLint directive-comments
    "eslint-comments/no-use": [
      "error",
      {
        "allow": [
          "eslint-disable",
          "eslint-disable-line",
          "eslint-disable-next-line",
          "eslint-enable"
        ],
      },
    ],

    //
    // eslint-plugin-import
    //

    // disallow non-import statements appearing before import statements
    "import/first": "error",
    // Require a newline after the last import/require in a group
    "import/newline-after-import": "error",
    // Forbid import of modules using absolute paths
    "import/no-absolute-path": "error",
    // disallow AMD require/define
    "import/no-amd": "error",
    // forbid default exports
    "import/no-default-export": "error",
    // Forbid the use of extraneous packages
    "import/no-extraneous-dependencies": [
      "error",
      {
        "devDependencies": true,
        "peerDependencies": true,
        "optionalDependencies": false
      },
    ],
    // Forbid mutable exports
    "import/no-mutable-exports": "error",
    // Prevent importing the default as if it were named
    "import/no-named-default": "error",
    // Prohibit named exports
    "import/no-named-export": "off", // we want everything to be a named export
    // Forbid a module from importing itself
    "import/no-self-import": "error",
    // Require modules with a single export to use a default export
    "import/prefer-default-export": "off" // we want everything to be named
  },
  "parserOptions": {
    "sourceType": "module",
    "ecmaFeatures": {
      "jsx": false
    },
    "project": "./tsconfig.json"
  },
  "overrides": [
    {
      "files": [
        "packages/eslint-plugin-tslint/tests/**/*.ts",
        "packages/eslint-plugin/tests/**/*.test.ts",
        "packages/parser/tests/**/*.ts",
        "packages/typescript-estree/tests/**/*.ts",
        "*.ts", "*.tsx"
      ],
      "env": {
       
      },
      
      "rules": {
        "jest/no-disabled-tests": "warn",
        "jest/no-focused-tests": "error",
        "jest/no-alias-methods": "error",
        "jest/no-identical-title": "error",
        "jest/no-jasmine-globals": "error",
        "jest/no-jest-import": "error",
        "jest/no-test-prefixes": "error",
        "jest/no-test-callback": "error",
        "jest/no-test-return-statement": "error",
        "jest/prefer-to-have-length": "warn",
        "jest/prefer-spy-on": "error",
        "jest/valid-expect": "error",
        "@typescript-eslint/explicit-function-return-type": ["error"]
      }
    },
    {
      "files": [
        "packages/eslint-plugin/tests/**/*.test.ts",
        "packages/eslint-plugin-tslint/tests/**/*.spec.ts"
      ],
      "rules": {
        "eslint-plugin/no-identical-tests": "error"
      },
    },
    {
      "files": [
        "packages/eslint-plugin/src/rules/**/*.ts",
        "packages/eslint-plugin/src/configs/**/*.ts",
        "packages/eslint-plugin-tslint/src/rules/**/*.ts"
      ],
      "rules": {
        // specifically for rules - default exports makes the tooling easier
        "import/no-default-export": "off"
      }
    },
    {
      "files": ["**/tools/**/*.ts", "**/tests/**/*.ts"],
      "rules": {
        // allow console logs in tools and tests
        "no-console": "off"
      }
    }
  ]
}